import AppNavbar from "./components/AppNavbar";
import Home from "./pages/Home"

import {Container} from "react-bootstrap";
import './App.css';

function App() {
  return (

    // In Reactjs, we normally render our components in an entry point or in a mother component called App. This is so we can group our components under a single entry point/main component

    // All other components/pages will be contained in our main component: <App />

    // ReactJS does not like rendering two adjacent elements. instead the adjacent element must be wrapped by a parent element/react fragment
    <>
      <AppNavbar />
      <Container fluid>
        <Home />
      </Container>
    </>
  );
}

export default App;
